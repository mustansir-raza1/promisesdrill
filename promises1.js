// Problem 1: Write a function that will return a particular board's information based on the boardID that is passed from the given list of boards in boards.json and then pass control back to the code that called it by using a callback function.

const fs = require("fs");
const getBoards = require("./boards.json");

function boardInfo(boardId){
    return new Promise ((resolve, reject)=>{
        const FoundId = getBoards.find((board)=>board.id == boardId) 
        if(FoundId){
            resolve(FoundId)
        }
        else{
            reject("error")
        }
    })
}
module.exports = boardInfo;