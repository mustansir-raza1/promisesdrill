//Problem 3: Write a function that will return all cards that belong to a particular list based on the listID that is passed to it from the given data in cards.json. Then pass control back to the code that called it by using a callback function.

const cardPath = require("./cards.json");
function cardsInfo(listId){
   return new Promise((resolve, reject)=>{
        const cardList = cardPath[listId]
        if(cardList){
            resolve(cardList)
        }
        else{
            reject("error")
        }
    })
}
module.exports = cardsInfo;