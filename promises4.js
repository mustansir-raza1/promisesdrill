// Problem 4: Write a function that will use the previously written functions to get the following information. You do not need to pass control back to the code that called it.

//     Get information from the Thanos boards
//     Get all the lists for the Thanos board
//     Get all cards for the Mind list simultaneously

const boardPath = require("./boards.json");
const listPath = require("./lists_1.json");
const cardPath = require("./cards.json");
const promises1 = require("./promises1.js");
const promises2 = require("./promises2.js");
const promises3 = require("./promises3.js");



function thanosInfo() {
    const thanosId = "mcu453ed";
    promises1(thanosId)
        .then((board) => {
            console.log(board);
        })
    promises2(thanosId)
        .then((list) => {
            console.log(list);
        })
    const mind = "qwsa221";
    promises3(mind)
        .then((mindlist) => {
            console.log(mindlist)
        })
        .catch((err) => {
            console.log(err);
        })
}
module.exports = thanosInfo;


