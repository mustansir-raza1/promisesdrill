// Problem 6: Write a function that will use the previously written functions to get the following information. You do not need to pass control back to the code that called it.

//     Get information from the Thanos boards
//     Get all the lists for the Thanos board
//     Get all cards for all lists simultaneously
const promises1 = require("./promises1.js")
const promises2 = require("./promises2.js")
const promises3 = require("./promises3.js")

function thanosInfo(){
    const thanosId = "mcu453ed";
    promises1(thanosId)
        .then((board) => {
            console.log(board);
        })
        .catch((err)=>{
            console.log(err)
        })
    promises2(thanosId)
        .then((list) => {
            console.log(list);
        })
        .catch((err)=>{
            console.log(err)
        })
    const cardslist = ["qwsa221","jwkh245","azxs123", "cffv432","ghnb768"];
    for(let list of cardslist){
        promises3(list)
    .then((list)=>{
        console.log(list)
    })

    }
    
}
module.exports = thanosInfo;