// Problem 2: Write a function that will return all lists that belong to a board based on the boardID that is passed to it from the given data in lists.json. Then pass control back to the code that called it by using a callback function.

const listPath = require("./lists_1.json");
function listInfo(listID){
    return new Promise ((resolve, reject)=>{
        const listFound = listPath[listID]
        if(listFound){
            resolve(listFound)
        }
        else{
            reject("error")
        }
    })
    
}
module.exports = listInfo;